# Graco

O **Graco** é o **Grupo de Gestores da Rede Acadêmica de Computação**. Ele é composto de professores, servidores técnico-administrativos e alunos (bolsistas e voluntários) que têm como objetivo manter e melhorar a rede de computadores e serviços de rede do **Instituto de Computação (IC)** da UFBA, de forma a apoiar as atividades acadêmicas do Instituto.

[[_TOC_]]

# O que estudar?

Você quer contribuir com o Graco mas não tem muito conhecimento técnico? Veja abaixo alguns materiais para estudo:

- [Guia Foca Linux](https://guiafoca.org/) - guia básico, intermediário e avançado de administração de sistemas Linux
- [Guia básico de Markdown](https://docs.pipz.com/central-de-ajuda/learning-center/guia-basico-de-markdown) -- linguagem de marcação usada para escrever documentação
- [Git](http://rogerdudler.github.io/git-guide/index.pt_BR.html) - sistema de controle de versão

# História

## Sites antigos (2000–2016)

- <https://wiki.dcc.ufba.br/Graco>
- <https://wiki.dcc.ufba.br/Intranet/WebHome>
- <https://wiki.dcc.ufba.br/RedeDCC/WebHome>
- <https://wiki.dcc.ufba.br/GracoIntranet/WebHome>
- <https://wiki.dcc.ufba.br/GAVRI/WebHome>

## Breve histórico

Em 2000, no então Instituto de Matemática, surgiu o Grupo de Administradores Voluntários da Rede de Informática (GAVRI), com enfoque no uso de software livre. O GAVRI foi responsável por instalar Linux em diversos computadores usados em laboratórios de pesquisa, dar utilidade a computadores obsoletos através do Linux Terminal Server Project (LTSP), implantar o TWiki para gerenciamento de conteúdo, instalar e configurar equipamentos de rede e elétricos, além de implantar e gerenciar serviços como email, FTP, lista de discussão, backup e servidor de impressão.

Em 2004 foi formada uma comissão de professores responsáveis pela rede, e em 2005 o GAVRI deu lugar ao grupo de Gestores da Rede Acadêmica de Computação (Graco), liderado pelo professor Guillaume Barreau. O Graco foi responsável por manter a infraestrutura anteriormente gerenciada pelo GAVRI, além de implantar e gerenciar novos serviços, como LDAP, Subversion, servidor de aplicações e sincronização de usuários em listas de discussão e outros serviços (sync-groups).

Em 2012 (ou antes), foi criado um grupo de trabalho formado por professores, o GT-REDES, para direcionar as ações do Graco. Em 2016, o Graco, então coordenado pelo professor Paul Regnier, foi desativado, devido a problemas como dificuldade de renovação dos membros do grupo, dificuldades para atender prontamente a requisições de usuários apenas com voluntários e divergência de opiniões entre os professores sobre decisões referentes à rede. 

O GAVRI e o Graco tiveram papel importante na formação dos alunos que participaram desses grupos, além de contribuir com o movimento de software livre na Bahia e no Brasil. Hoje, alguns dos ex-membros do Graco e do GAVRI desempenham funções importantes na Superintendência de Tecnologia da Informação (STI) da UFBA, no Ponto de Presença da RNP na Bahia (PoP-BA), e outros atuam em outras funções importantes dentro e fora do Brasil.
